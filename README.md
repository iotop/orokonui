
# Orokonui Project


Orokonui Bird Sanctuary is our client for this project.  We have developed hardware and software to help rangers monitoring the status of the gates within the sanctuary in order to keep the birds safe from being hunted by predators.  Our team has developed hardwares using reed switches sending the status of each gate by using mDot devices.  The status of each gate is sent from mDot device to installed Orokonui RAK Gateway in Orokonui shed and our local database receiving data from Orokonui gateway via Rooftop gateway at Polytech building.  Each gate has a sending interval of 15 minutes unless the status is changed before the interval time.  We have deployed two gates at Orokonui in semester 2, 2019; orokonui1 and orokonui3.

Our IoT team has also developed web application for external access outside Polytechnic so that the client can view the status of each gate without physically check the gate.  Ultimately, the aim of IoT device is to enhance quality and productivity lifestyle of end users.

### Progress for Orokonui Project on 2020, Semester 1
- Deployed Orokonui development server pulled from dev branch on gitlab.
- Display only functional links on the sidebar to avoid confusion for users.
- Deployed dev Orokonui server inside docker container using the same server as Heatmap under reversed proxy (using the same server, different ports and name).  
- Development server web application can only be accessed via  http://orokonui.example.com/Orokonui/  inside OP after adding IP address inside host file on local machine.   
- Changed batteries on both deployed devices at Orokonui on 06/06/2020.  
- Fixed wires on Orokonui 3 and installed a piece of wood board to protect our wires from being eaten by birds.
- Only deployed devices are registered under op_orokonui TTN application.  mDot 4 is registered under roomsensor_dev for testing purpose.  Others will need to be re-registered, documentation on how to use mDot is in this repository inside node folder.


