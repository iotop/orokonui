# Orokonui Web App

## Overview

The Orokonui Web App was designed to display different gates around the Orokonui Ecosanctuary in the outskirts of Dunedin.
The app itself runs on a Django framework, and is run through the VM on an Apache service.

The app gets the data for the gates using a MQTT Broadcast, looking out for the names of the provided gates on the network.
Once an update is received, it changes the gate's status on the map.

Currently, the service is under heavy development and only one gate is currently being searched for.


## Accessing and Updating the App

To access the Web App's root folder on the VM, there are a few steps to follow.

The App is run within a Docker Container, so this simplifies a few things compared to running a basic Apache server.

First, you will need to create a session to the server, the credentials are found in the sensitive repository.
I recommend using MobaXterm to create the session, since it provides a GUI to interact with the VM unlike PuTTy.

Once you have created a session with the VM, you need to navigate to the root directory of the Web App. You can do this by using the following commands:

```
cd ..
cd ..
cd srv/Docker/orokonui-web-app
```

Once you're in the root directory for the Web App, you can use git to update the files to the latest version

```
git pull
```

Once you've pulled any required files from the repository, you will need to update the current build within the container. This is achieved with the following commands:

```
docker-compose build
docker-compose up
```

Make sure you are in the `orokonui-web-app\Projectenv\Project` folder, or it will not work.

When you run the build command, you should be greeted with a sight similar to this:

![Terminal Docker Build](DocumentationImages/TerminalBuild.png)

Once the build is completed, you can run the 'up' command, and the app should run on the latest version.

## Cloning repository to your local machine

First of all, please ensure you have access to our team repository in order to clone the repository locally.  See Dev-Ops team or IOT lecturer for your access.
It is also important to have your public ssh key on hand in order to clone.  GitLab supports RSA, DSA, ECDSA, and ED25519 keys.  Your existing SSH key should be in .ssh folder within your H: drive.  You could also create a new SSH key by
```
ssh-keygen -t ed25519 -C "email@example.com"
```

Or, if you want to use RSA:
```
ssh-keygen -t rsa -b 4096 -C "email@example.com"
```

Adding an SSH key to your GitLab Account

```
cat ~/.ssh/id_ed25519.pub | clip
```
Or go to your local .ssh folder and use text editor to open .pub file to copy your ssh keys.

Open your GitLab webpage, login and go to Settings and look for the side tab to the left to find keys symbol (SSH Keys).  

![SSH Key](DocumentationImages/sshkeytab.png)


Add/Paste your SSH key to SSH key box and add a preferred name for your SSH key.

![SSH Key](DocumentationImages/sshkey1.png)

Now you should have access to clone/pull from team repository.  You could now navigate to the preferred folder within your computer and clone the repo.

```
git clone git@gitlab.op-bit.nz:BIT/Project/Internet-Of-Things/orokonui.git
```
![SSH Key](DocumentationImages/gitClone.png)

## Running a Local Version of the App

To run a local version of the app, there are a few things you will have to set up. For the easiest set-up, You can set up a local environment with Anaconda Navigator.
The installation instructions for it can be found Below:

Windows: [Windows Install](https://docs.anaconda.com/anaconda/install/windows/)
Macintosh: [Mac OS Install](https://docs.anaconda.com/anaconda/install/mac-os/)
Linux: [Linux Install](https://docs.anaconda.com/anaconda/install/linux/)

Once you've downloaded and installed Anaconda Navigator, Open it up and go to the Environments Tab

![Anaconda Environments](DocumentationImages/anacondaEnvironments.png)

## Creating a New Environment

Create a New Environment for the Web App by clicking the Create button at the bottom of the page.

![Anaconda Create](DocumentationImages/anacondaCreateButton.png)

Give the Environment a relevant name, and make sure the checkbox for the Package Python 3.7 is selected. Then click Create.

Once the Environment is created and done loading the required packages, select it in the Environments list, and click the Arrow, which will bring up a drop-down list.
Select "Open Terminal" From the list, You'll be greeted by a CLI, with a line showing the name of your environment, and it's current directory it's in.

## Setting up the Environment

Change the directory to the required one using the `cd` Command. If required directory is on another Drive, simply type the name of the drive. For example, if the required
directory is on the H:\ drive, just type `H:`
From there, you can type `cd H:\link\to\the\directory` replacing the folder hierarchy with the correct one for your application.

![Anaconda Environments](DocumentationImages/anacondaTerminal.png)

## Installing Dependencies

Once you're in the required directory, you won't be able to just instantly start the app like you would with the current Live Site's Virtual Machine. You'll need to download a few
requirements for your Environment before it can run the app.

The required modules, along with the code to install them for the environment are as follows:
```
pip install -r requirements.txt
```
**Note:** Remember to be in the folder that contains the requirements.txt file, or include the URL in the command: `\orokonui-web-app\Projectenv\Project`

Copy `secrets.py` file from Orokonui folder and place the file in `Projectenv/Project/Project` 

After those have been installed, you can run the web app on your local environment with the following command: `python manageDevelopment.py runserver`

**Note:** If you have previously installed Django from an installer please ensure it is version 2.2.10 or the application may not run when using python commands.

**Note:** Make sure you're navigated to the "orokonui-web-app\Projectenv\Project" folder before running the command.

![Anaconda Environments](DocumentationImages/anacondaTerminal.png)

Once the server has loaded everything, you can view the localized Web App on `localhost:8000/Orokonui`
The login details are the same as on the live site, and can be found in the sensitive repository.