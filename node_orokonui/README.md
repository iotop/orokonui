# mDot-GateMonitor-AU915  

## mDot using a Reed switch

#### Prep

Must already have mbed.org account set up. If that is not done, see the Prep section of this file: https://gitlab.com/iotop/nodes/-/tree/master/Adafruit-ABP-HelloWorld-AU915


#### Required Hardware

mDot - 915 MultiTech mDot UDK board with Reed Switch 

#### Wiring 

Using the corresponding Arduino pins (D0 and GND) on the UDK and some photos below of what the board should look like:

![alt text](img/gate_fritz.JPG)

![alt text](img/gate_pic1.png)

![alt text](img/gate_pic2.jpg)

#### Importing the code 

1. Go to https://os.mbed.com/users/lootspk/code/mDot-GateMonitor-AU915/ and import the code into the compiler and upload it to the board, click import into compiler to make it editable by you and able to be compiled 
(make sure that you have set the sensor pin accordingly).

![alt text](img/importintocompiler.jpg)
Once in the compiler click "import as program"

![alt text](img/importasprogram.jpg)

Near the top of the program, change the network setting values to match the settings required for your LoRaWAN.
![alt text](img/networksettings.jpg)

- Please ensure to transfer the mDot device with the correct corresponding TTN device address, network session key and app session key to the registered devices on op_orokonui TTN application.

If adaptive data rate is disabled, ie, ```static bool adr = false;```

Scroll down to ```dot->setTxDataRate(mDot::DR2);``` and set your required data rate.  This will determine the range and gain of your device to the gateway in Orokonui.  In 2019 Semester 2, Orokonui3 is using DR1 whereas Orokonui1 is using DR2.


![alt main_mdot.png](node_orokonui/img/main_mdot.png "mdot main image")

- Adjust code in main.cpp according to your need as well as dot_util.cpp only if necessary.

- Click “Compile” and download the bin file when prompted.

![alt text](img/savebin.jpg)

- After downloading the files, the newly compiled .bin folder should override the original one inside the mDot.

** Notes ** : Please find a sample of main.cpp file in this folder called 'mdot-sample.cpp'



##### Load program onto mDot

Attach the mDot to the UDK board, and attach the UDK to a PC via USB.
The mDot should appear in the pc as a flash drive would for example “MULTITECH (F:)”.
Copy the binary file to this drive. The mDot usually would automatically restart, but if it does not, press the Reset button on the UDK.

#####  Seeing debug info on a PC over USB

On Windows you must install serial-USB driver from here: http://www.st.com/en/embedded-software/stsw-link009.html

Open a serial terminal. I used the Arduino IDE’s serial monitor set to the correct COM port (this varies depending on the machine) at 9600 baud.

If the application runs correctly and the serial monitor is configured correctly you should see debugging output, and be receiving packets at the gateway. 



# Orokonui Devices
- This image displays one of the gate prototypes using mDot.  Data is sent to TTN server when the sensor detects a change in gate status and update status every 15 minutes.
      
![alt prototype.jpeg](node_orokonui/img/prototype.jpeg "mdot prototype image")

# Fritz Diagram
- To wire a new mDot node, follow this [diagram](node_orokonui/sensor-mdot.pdf).


# Register a new TTN device for this project

The device is required to be registered within the correct application to communicate with TTN.  

- Go to https://console.thethingsnetwork.org and use team credential from our **Sensitive repository** called "The Things Network" to login.  

- Once you are logged in, click on **Applications** to find a list of all  applications.  Select **op_orokonui**.

- Click on the **register device** icon on the **Devices** panel and enter _Device ID_ for your device. 

- Select the customise icon on the Device EUI form to activate the automatic code generator, Device EUI and App Key will be automatically generated for you.  Click **Register** to complete your initial device registration.  

- Go to **Settings** tab, look for **Activation Method** and select **ABP** and ensure to select **16 bit** on the **Frame Counter Width** and **uncheck Frame Counter Checks** and save your settings.

![alt 16bits.png](img/16bits.png "uncheck frame counter")


# Troubleshoot
- One of the indications that the mDot device you are using is defective is the failure of files transfer.  The error may appear as the device does not have enough space to copy files.

- After plugging your mdot to Multitech and to a USB port, you should see mdot icon displays on your desktop.  If you are unable to see it, make sure your USB connector is not only for charging but also data transmission.  
