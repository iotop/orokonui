# Orokonui Deployment
This guide will assume you've already setted up the surrounding compenents of this container. If you have not then please consult those guides to set them up.

## Requirements
- 1~GB Memory minimum
- Ports 80 & 443 open
- Ubuntu LTS (Any version)
- 30GB storage at least
- An existing reverse proxy container (we'll assume it's nginx)
- Things network account, devices and integrations already set up
- Docker and Docker-compose has already been installed
- User has been added to the docker group (or has sudo)

## Installation
Simply clone/move this directory into your working directory
Make changes to the following files.

- .env (Self explanatory)
- ttn/connect.inc.php (Database Username and Password)
- Project/secrets.py (Things Network secret key that you can find in the integrations of the app on their website)
- Project/.env (DB Username and Password again)

- Run the following commands to build and run it.
``` bash
docker-compose build
docker-compose up -d
```
OR `docker-compose build && docker-compose up -d`

Go into your reverse proxy and add a new entry to redirect to the new hostname. An example has been provided at the end of this document.

Migrate the database using the following command:
```bash
docker exec -i orokonui-db sh -c 'exec mysql -uroot -p"rootpwdhere"' < migrations.sql
```

If everything goes right there will be a new website and all ttn requests will go through your ingest url.

## Common problems
If you recieve errors regarding ports make sure the server's nginx/apache has been disabled as some of them are enabled by default. Use the following commands if you need to.
`sudo systemctl disable nginx.server`
`sudo systemctl disable apache.server`


## Examples

```bash
# orokonui config.
# We keep the ingest and main website seperate for diagnosis reasons.
server {
  listen 80;
  listen 443;
  server_name orokonui.example.nz;

  location / {
    include /etc/nginx/includes/proxy.conf;
    proxy_pass http://orokonui-nginx:8000;
  }

  access_log off;
  error_log  /var/log/nginx/error.log error;
}

# Orokonui ingest
server {
  listen 80;
  listen 443;
  server_name orokonuiingest.example.nz;

  location / {
    include /etc/nginx/includes/proxy.conf;
    proxy_pass http://orokonui-ingest;
  }

  access_log off;
  error_log  /var/log/nginx/error.log error;
}
```
