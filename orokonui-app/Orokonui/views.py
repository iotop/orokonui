from django.shortcuts import render, HttpResponse
from django.shortcuts import render, redirect
from django.contrib import auth
import requests
import json
import os
import base64
import mysql.connector
import datetime
import environ
from .models import Gate
from django.contrib.auth.decorators import login_required
from mysql.connector import Error, DatabaseError
from itertools import chain
from django.http import HttpResponse

env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)
# reading .env file
environ.Env.read_env()

# ENV Variables
DB_USER = env('DB_USER')
DB_PASS = env('DB_PASS')
IP = env('DB_IP')
PORT = env('DB_PORT')
DB = env('DATABASE')

#Decoding Functions
def base64ToString(encoded):
    # Decode Function that returns a decoded version of the input, if it requires it
    # Otherwise, returns the input without decoding it (in-case it tries to decode plaintext)
    try:
        return base64.b64decode(encoded).decode('utf-8')
    except:
        print("Could not decode the inputted string", encoded)
        return encoded

#retrieve data from database
def SQLConnection(gates):
    try:
        connection = mysql.connector.connect(host=IP,
                                        database=DB,
                                        user=DB_USER,
                                        password=DB_PASS)
        if connection.is_connected():
            db_Info = connection.get_server_info() # Get the Server Information
            print("Connected to MySQL Server version ", db_Info) # Debug info that shows where the app is connecting to

            searchQuery = """
            select data.dev_id, payload, time from data
            inner join (
              SELECT data.dev_id, max(time) as max_time from data where app_id = 'op_orokonui'
              GROUP by dev_id
            ) devices
            on data.dev_id = devices.dev_id
              and data.time = max_time
            where app_id = 'op_orokonui'
            """
            # searchQuery = "SELECT d1.dev_id, d1.payload, d1.time FROM data d1 LEFT JOIN data d2 ON (d1.dev_id = d2.dev_id) WHERE d2.app_id = 'op_orokonui' AND d1.time = (SELECT MAX(d2.time) FROM data d2 WHERE d1.dev_id = d2.dev_id) GROUP BY d1.dev_id"
            # Set search query for all nodes relating to orokonui, selecting the top 100 and ordering by time in a descending order
            cursor = connection.cursor() # Create a cursor for the connection to the database
            cursor.execute(searchQuery) # Execute the Search Query defined above
            records = cursor.fetchall()# Fetch all records from the search query

            for device in records:
                entry = {} #declare empty dictionary
                entry['id'] = device[0] # Save The Device's Name into entry[0]
                # print(base64ToString(device[1]))
                entry['status'] = int(base64ToString(device[1])) # Get Integer of Decoded Payload from Gate and save to entry[1]
                datetime = str(device[2]).split(" ") # Split the date and time from the packet
                entry['date'] = datetime[0] # Save the Date
                entry['time'] = datetime[1] # Save the Time
                gates.append(entry)
                print(entry)

    except Error as e:
        print(str(e))
    return gates

@login_required(login_url='login')
def home(request):
    return render(request, 'Orokonui/home.html')

@login_required(login_url='login')
def data(request):
    try:
        gates = [] # Create an empty array of gates
        gateArray = SQLConnection(gates)
        gateCount = Gate.objects.all().count()
        context = {
            'gateCount': gateCount,
            'gates': gateArray
        }
    except Error as e:
        return e
    return render(request, 'Orokonui/data.html', context=context)

def dataMap(gate=None): # Function for getting information for the map (API)
    gates = [] # Create an empty array of gates
    gateArray = SQLConnection(gates)
    gate_list = json.dumps(gateArray)
    return HttpResponse(gate_list)
