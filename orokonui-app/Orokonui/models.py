from django.db import models
from datetime import datetime


class Gate(models.Model):
    isOpen = models.BooleanField(default=True)
    whenOpen = models.DateTimeField(default=datetime.now())
