var gateStatus = "";

$(document).ready(function () {
    client.connect(options);
});

function b64DecodeUnicode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}



var dataString = ""

//Using the HiveMQ public Broker, with a random client Id
//var client = new Messaging.Client("broker.mqttdashboard.com", 8000, "myclientid_" + parseInt(Math.random() * 100, 10));

//var client = new Messaging.Client("10.25.100.49", 1884, "WebAppTester" );
var client = new Messaging.Client("iot.op-bit.nz", 1884, "myclientid_" + parseInt(Math.random() * 100, 10));
var jsonData = ""

//Gets  called if the websocket/mqtt connection gets disconnected for any reason
client.onConnectionLost = function (responseObject) {
    //Depending on your scenario you could implement a reconnect logic here
    alert("connection lost: " + responseObject.errorMessage);
};

//Gets called whenever you receive a message for your subscriptions
client.onMessageArrived = function (message) {
    //Do something with the push message you received
    console.log("Got a message, it arrived!");
    jsonData = message.payloadString
    jsonData = jsonData.substring(jsonData.indexOf("{"));
    jsonData = JSON.parse(jsonData)
    console.log(jsonData)
    var dataVal = jsonData["payload_raw"]
    var deviceName = jsonData["dev_id"]
    console.log("Data",dataVal,"Name",deviceName)
    dataVal = b64DecodeUnicode(dataVal)
    console.log("Decoded String:"+dataVal)

    var d = Date();
    console.log(d);
    

    gates[deviceName].closed = (dataVal == 11);
    document.getElementById(deviceName).innerHTML = gates[deviceName].closed ? '<span class="label bg-green">Closed</span>' : '<span class="label bg-red">Open</span>';

//    switch (deviceName) {
//         case "gate-3D":
//             console.log("OpenGate" + deviceName)
//             if (dataVal == 1) {
//                 console.log("Gate is closed.")
//                 gateOneStatus = false;
//                 document.getElementById("gateOne").innerHTML = '<span class="label bg-green">Closed</span>';
//             }
//             else {
//                 console.log("Gate is open.")
//                 gateOneStatus = true;
//                 document.getElementById("gateOne").innerHTML = '<span class="label bg-red">Open</span>';
//             }

//             break;
//     }
    updateMap();
};

//Connect Options
var options = {
    timeout: 3,
    //Gets Called if the connection has sucessfully been established
    onSuccess: function () {
        client.subscribe('ttn/op_orokonui/#', { qos: 2 });
    },
    //Gets Called if the connection could not be established
    onFailure: function (message) {
        alert("Connection failed: " + message.errorMessage);
    }
};

//Creates a new Messaging.Message Object and sends it to the HiveMQ MQTT Broker
var publish = function (payload, topic, qos) {
    //Send your message (also possible to serialize it as JSON or protobuf or just use a string, no limitations)
    var message = new Messaging.Message(payload);
    message.destinationName = topic;
    message.qos = qos;
    client.send(message);
}