<?php
    include 'connect.inc.php';	// Database Credentials

    // Receive the RAW post data.
    $content = trim(file_get_contents("php://input"));

    // Attempt to decode the incoming RAW post data from JSON.
    $decoded = json_decode($content);

	if ($decoded == NULL)
	{
		echo("No POST Data!");
	}
	else
	{
		// Insert
		$insertQuery="INSERT INTO data (dataID, time, app_id, dev_id, hardware_serial, payload, frequency, data_rate, channel, rssi) VALUES (NULL, current_timestamp(), :app_id, :dev_id, :hardware_serial, :payload, :frequency, :data_rate, :channel, :rssi)"; // Template SQL Query
		$stmt =$pdo->prepare($insertQuery);

		$stmt->bindParam(':app_id', $decoded->app_id);
		$stmt->bindParam(':dev_id', $decoded->dev_id);
		$stmt->bindParam(':hardware_serial', $decoded->hardware_serial);
		$stmt->bindParam(':payload', base64_decode($decoded->payload_raw));
		$stmt->bindParam(':frequency', $decoded->metadata->frequency);
		$stmt->bindParam(':data_rate', $decoded->metadata->data_rate);
		$stmt->bindParam(':channel', $decoded->metadata->gateways[0]->channel);
		$stmt->bindParam(':rssi', $decoded->metadata->gateways[0]->rssi);

		$stmt->execute();    // Inserts row into table
	}


?>

